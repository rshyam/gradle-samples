package com.example.gradle;

import org.junit.Test;
import static org.junit.Assert.*;

public class TestJavaClass {
	private JavaClass underTest = new JavaClass();
	
	@Test
	public void isEmptyWorks(){
		assertTrue(underTest.myIsEmpty(""));
		assertFalse(underTest.myIsEmpty("bla"));
	}
}
